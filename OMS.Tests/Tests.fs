namespace OMS.Core.Tests

open NUnit.Framework

open OMS.Core

module Tests =

    [<SetUp>]
    let Setup () =
        ()

    [<Test>]
    let TestCreateLimitOrder () =
        let order = Order.createLimit OrderSide.Buy "AAPL" 1000 (decimal 200.0) GoodForDay
        match order with
        | Ok _ -> Assert.Pass()
        | Error message -> Assert.Fail(message)
    
    [<Test>]
    let TestCreateLimitOrderWithZeroPrice () =
        let order = Order.createLimit OrderSide.Buy "AAPL" 1000 (decimal 0.0) GoodForDay
        match order with
        | Ok _ -> Assert.Fail()
        | Error _ -> Assert.Pass()
    
    [<Test>]
    let TestCreateLimitOrderWithNegativePrice () =
        let order = Order.createLimit OrderSide.Buy "AAPL" 1000 (decimal 0.0) GoodForDay
        match order with
        | Ok _ -> Assert.Fail()
        | Error _ -> Assert.Pass()

    [<Test>]
    let TestCreateLimitOrderWithZeroQuantity () =
        let order = Order.createLimit OrderSide.Buy "AAPL" 0 (decimal 200.0) GoodForDay
        match order with
        | Ok _ -> Assert.Fail()
        | Error _ -> Assert.Pass()
    
    [<Test>]
    let TestCreateLimitOrderWithNegativeQuantity () =
        let order = Order.createLimit OrderSide.Buy "AAPL" -20 (decimal 200.0) GoodForDay
        match order with
        | Ok _ -> Assert.Fail()
        | Error _ -> Assert.Pass()
    
    
    [<Test>]
    let TestCreateMarketOrder () =
        let order = Order.createMarket OrderSide.Sell "AAPL" 1000
        match order with
        | Ok _ -> Assert.Pass()
        | Error message -> Assert.Fail(message)
    