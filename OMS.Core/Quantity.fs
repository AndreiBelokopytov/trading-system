namespace OMS.Core

type Quantity = private Quantity of int

module Quantity =
    let create quantity =
        if quantity <= 0 then "Invalid Quantity value" |> Error
        else quantity |> Quantity |> Ok
