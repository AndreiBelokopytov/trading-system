namespace OMS.Core

type Price = private Price of decimal

module Price =
    let create price =
        if price <= decimal 0 then "Invalid Price value" |> Error
        else price |> Price |> Ok 

