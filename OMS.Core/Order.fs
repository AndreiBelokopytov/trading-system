namespace OMS.Core

open System

type OrderSide = | Buy | Sell

type OrderStatus = | Created | New | Canceled

type TiF = | GoodForDay | FillOrKill

type OrderDetails = private {
    Side: OrderSide
    Ticker: String
    Quantity: Quantity
}

type LimitOrder = private {
    Timestamp: DateTime
    Details: OrderDetails
    Price: Price
    TiF: TiF
}

type MarketOrder = private {
    Timestamp: DateTime
    Details: OrderDetails
}

type Order = private | LimitOrder of LimitOrder | MarketOrder of MarketOrder

module Order =
    let createLimit orderSide ticker quantity price tif =
        match quantity |> Quantity.create with
        | Error message -> Error message
        | Ok quantity ->
            match price |> Price.create with
            | Error message -> Error message
            | Ok price ->
                Ok {
                    Details = {
                        Side = orderSide
                        Ticker = ticker
                        Quantity = quantity
                    }
                    Timestamp = DateTime.Now
                    Price = price
                    TiF = tif
                }
    
    let createMarket orderSide ticker quantity =
        match quantity |> Quantity.create with
        | Error message -> Error message
        | Ok quantity ->
            Ok {
                Details = {
                    Side = orderSide
                    Ticker = ticker
                    Quantity = quantity
                }
                Timestamp = DateTime.Now
            }
